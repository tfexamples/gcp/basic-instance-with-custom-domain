
resource "google_dns_managed_zone" "domain-zone" {
  name        = "${var.project_id}-zone"
  dns_name    = "${var.domain}."
  description = "${var.domain} Zone"
  labels = {
    environment = "testing"
  }
}

resource "google_dns_record_set" "instance-a-recordset" {
  #provider     = "google-beta"
  managed_zone = google_dns_managed_zone.domain-zone.name
  name         = "www.${var.domain}."
  type         = "A"
  rrdatas      = [google_compute_instance.basic_instance.network_interface[0].access_config[0].nat_ip]
  ttl          = 86400

  depends_on = [
    google_compute_instance.basic_instance,
  ]
}
