variable "project_id" {
  type        = string
  description = "The GCP Project ID"
}

variable "ssh_user" {
  type        = string
  description = "The ssh user who will have access to the instances"
}

variable "domain" {
  type        = string
  description = "Domain to configure Cloud DNS"
}

variable "region" {
  type        = string
  description = "Region to configure Cloud DNS"
  default     = "us-east1"
}

variable "zone" {
  type        = string
  description = "Zone to configure Cloud DNS"
  default     = "us-east1-b"
}

variable "gcp_credentials_path" {
  type        = string
  description = "GcpCredentialsPath to configure Cloud DNS"
  default     = "~/.gcp/compute-service-account.json"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "n1-standard-1"
}
